# vue-datagrid-case-study
A basic data-grid for case-study.

## Features
- Drag-drop column order
- Multiple sorter (with shift-key)
## Install Dependencies
```
yarn install
```
## Run
```
yarn serve
```



